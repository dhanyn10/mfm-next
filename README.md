# mfm-next

## Project setup
```
npm install
```

### Compiles for development
```
npm run electron:serve
```

### Compiles and minifies for production
```
npm run electron:build
```
